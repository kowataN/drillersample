﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class DrillerSystem : MonoBehaviour
{
    [SerializeField] GameObject _BlockPrefab = default;
    [SerializeField] GameObject _PlayerPrefab = default;

    const int MAP_WIDTH = 10 + 2;
    const int MAP_HEIGHT = 10;

    const int PLAYER_WIDTH = 1;
    const int PLAYER_HEIGHT = 1;

    /// <summary>
    /// ブロックの状態
    /// </summary>
    public enum EBlockState { kNone, kPlayer, kFrame, kSkyBlue, kYellow, kPurple, kBlue, kOrenge, kGreen, kRed, kMax };

    private readonly EBlockState[,] _InitMap = new EBlockState[MAP_HEIGHT, MAP_WIDTH] {
        { EBlockState.kFrame, EBlockState.kBlue,   EBlockState.kBlue,   EBlockState.kRed,     EBlockState.kRed,     EBlockState.kNone,   EBlockState.kNone,    EBlockState.kPurple, EBlockState.kOrenge,  EBlockState.kOrenge,  EBlockState.kBlue,    EBlockState.kFrame },
        { EBlockState.kFrame, EBlockState.kBlue,   EBlockState.kYellow, EBlockState.kYellow,  EBlockState.kRed,     EBlockState.kNone,    EBlockState.kNone,   EBlockState.kPurple, EBlockState.kPurple,  EBlockState.kRed,     EBlockState.kOrenge,  EBlockState.kFrame },
        { EBlockState.kFrame, EBlockState.kBlue,   EBlockState.kGreen,  EBlockState.kSkyBlue, EBlockState.kSkyBlue, EBlockState.kYellow,  EBlockState.kOrenge, EBlockState.kPurple, EBlockState.kRed,     EBlockState.kNone,    EBlockState.kOrenge,  EBlockState.kFrame },
        { EBlockState.kFrame, EBlockState.kPurple, EBlockState.kGreen,  EBlockState.kGreen,   EBlockState.kSkyBlue, EBlockState.kYellow,  EBlockState.kOrenge, EBlockState.kYellow, EBlockState.kOrenge,  EBlockState.kNone,    EBlockState.kNone,    EBlockState.kFrame },
        { EBlockState.kFrame, EBlockState.kPurple, EBlockState.kGreen,  EBlockState.kBlue,    EBlockState.kGreen,   EBlockState.kSkyBlue, EBlockState.kBlue,   EBlockState.kRed,    EBlockState.kOrenge,  EBlockState.kNone,    EBlockState.kSkyBlue, EBlockState.kFrame },
        { EBlockState.kFrame, EBlockState.kPurple, EBlockState.kPurple, EBlockState.kRed,     EBlockState.kGreen,   EBlockState.kBlue,    EBlockState.kBlue,   EBlockState.kRed,    EBlockState.kSkyBlue, EBlockState.kSkyBlue, EBlockState.kSkyBlue, EBlockState.kFrame },
        { EBlockState.kFrame, EBlockState.kOrenge, EBlockState.kOrenge, EBlockState.kRed,     EBlockState.kRed,     EBlockState.kPurple,  EBlockState.kPurple, EBlockState.kYellow, EBlockState.kNone,    EBlockState.kSkyBlue, EBlockState.kNone,    EBlockState.kFrame },
        { EBlockState.kFrame, EBlockState.kOrenge, EBlockState.kOrenge, EBlockState.kBlue,    EBlockState.kBlue,    EBlockState.kPurple,  EBlockState.kYellow, EBlockState.kYellow, EBlockState.kRed,     EBlockState.kBlue,    EBlockState.kNone,    EBlockState.kFrame },
        { EBlockState.kFrame, EBlockState.kRed,    EBlockState.kRed,    EBlockState.kGreen,   EBlockState.kBlue,    EBlockState.kRed,     EBlockState.kRed,    EBlockState.kBlue,   EBlockState.kRed,     EBlockState.kBlue,    EBlockState.kNone,    EBlockState.kFrame },
        { EBlockState.kFrame, EBlockState.kFrame,  EBlockState.kFrame,  EBlockState.kFrame,   EBlockState.kFrame,   EBlockState.kFrame,   EBlockState.kFrame,  EBlockState.kFrame,  EBlockState.kFrame,   EBlockState.kFrame,  EBlockState.kFrame,    EBlockState.kFrame }
    };


    // ブロックの実態
    private GameObject[,] _MapBlockObj = new GameObject[MAP_HEIGHT, MAP_WIDTH];
    private Block[,] _MapBlock = new Block[MAP_HEIGHT, MAP_WIDTH];

    // ブロックの状態
    private EBlockState[,] _MapBlockState = new EBlockState[MAP_HEIGHT, MAP_WIDTH];
    // 最終的なブロックの状態
    private EBlockState[,] _MapBlockStateFinal = new EBlockState[MAP_HEIGHT, MAP_WIDTH];

    private bool[,] _SearchMap = new bool[MAP_HEIGHT, MAP_WIDTH];

    // プレイヤー
    private EBlockState[,] _PlayerBlockState = new EBlockState[PLAYER_HEIGHT, PLAYER_WIDTH];
    private int _PlayerX = 5;
    private int _PlayerY = 0;

    //　落下
    private const float _DropInterval = 1.0f;
    private float _DropTimer = _DropInterval;

    struct PosTbl
    {
        public int x;
        public int y;
        public PosTbl(int posX, int posY)
        {
            this.x = posX;
            this.y = posY;
        }
    };
    List<PosTbl> _RemoveList = new List<PosTbl>();

    // Start is called before the first frame update
    void Start()
    {
        // 初期状態
        for (int y = 0; y < MAP_HEIGHT; y++)
        {
            for (int x = 0; x < MAP_WIDTH; x++)
            {
                // ブロックの実体
                GameObject newObj;
                if (_MapBlockState[y, x] == EBlockState.kPlayer)
                {
                    newObj = GameObject.Instantiate<GameObject>(_PlayerPrefab);
                }
                else
                {
                    newObj = GameObject.Instantiate<GameObject>(_BlockPrefab);
                }

                Block newBlock = newObj.GetComponent<Block>();
                newObj.transform.localPosition = new Vector3(x, MAP_HEIGHT - y, 0);
                _MapBlockObj[y, x] = newObj;
                _MapBlock[y, x] = newBlock;
                // ブロックの状態
                _MapBlockState[y, x] = _InitMap[y, x];
                _MapBlockStateFinal[y, x] = _MapBlockState[y, x];
            }
        }

        StartMove();
    }

    // Update is called once per frame
    void Update()
    {
        AutoDrop();
        PlayerMove();
        BlockRemove();

        // 状態反映
        UpdateBlockState();
    }

    void UpdateBlockState()
    {
        for (int y = 0; y < MAP_HEIGHT; y++)
        {
            for (int x = 0; x < MAP_WIDTH; x++)
            {
                // ブロックの状態
                _MapBlockStateFinal[y, x] = _MapBlockState[y, x];
            }
        }

        for (int y = 0; y < PLAYER_HEIGHT; y++)
        {
            for (int x = 0; x < PLAYER_WIDTH; x++)
            {
                // ブロックの状態
                _MapBlockStateFinal[_PlayerY + y, _PlayerX + x] = _PlayerBlockState[y, x];
            }
        }

        for (int y = 0; y < MAP_HEIGHT; y++)
        {
            for (int x = 0; x < MAP_WIDTH; x++)
            {
                // ブロックの状態
                _MapBlock[y, x].SetState(_MapBlockStateFinal[y, x]);
            }
        }

    }

    void StartMove()
    {

        for (int y = 0; y < PLAYER_HEIGHT; y++)
        {
            for (int x = 0; x < PLAYER_WIDTH; x++)
            {
                // ブロックの状態
                _PlayerBlockState[y, x] = EBlockState.kPlayer;

            }

        }
    }

    bool CheckPlayerMove(int offsetX, int offsetY)
    {
        int posX = _PlayerX + offsetX;
        int posY = _PlayerY + offsetY;

        if (posX < 0 || posX >= MAP_WIDTH || posY < 0 || posY >= MAP_HEIGHT)
        {
            return false;

        }
        return (_MapBlockState[posY, posX] == EBlockState.kNone);
    }

    void AutoDrop()
    {
        _DropTimer -= Time.deltaTime;
        if (_DropTimer <= 0.0f)
        {
            if (CheckPlayerMove(0, 1))
            {
                _PlayerY++;
                _DropTimer = _DropInterval;

            }
        }
    }

    void PlayerMove()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (CheckPlayerMove(-1, 0))
            {

                _PlayerX--;
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (CheckPlayerMove(1, 0))
            {
                _PlayerX++;
            }

        }
    }


    void InitSearchMap()
    {
        for (int y = 0; y < MAP_HEIGHT; y++)
        {
            for (int x = 0; x < MAP_WIDTH; x++)
            {
                // ブロックの状態
                _SearchMap[y, x] = false;
            }
        }

    }
    void BlockRemove()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            EBlockState targetState = _MapBlockState[_PlayerY + 1, _PlayerX];
            //Debug.Log("target : " + targetState.ToString());
            if (targetState != EBlockState.kNone)
            {
                SearchBlock(targetState);
                if (_RemoveList.Count > 0)
                {
                    foreach (PosTbl Pos in _RemoveList)
                    {
                        SetBlockState(Pos.x, Pos.y, EBlockState.kNone);
                    }
                    _RemoveList.Clear();
                }
            }
        }
    }

    void SetBlockState(int x, int y, EBlockState state)
    {
        _MapBlockState[y, x] = state;
    }

    void SearchBlock(EBlockState state)
    {
        _RemoveList.Clear();
        InitSearchMap();
        SearchTargetBlock(_PlayerX, _PlayerY + 1, state);

    }

    void SearchTargetBlock(int offsetX, int offsetY, EBlockState state)
    {
        if (offsetX < 0 || offsetX >= MAP_WIDTH || offsetY < 0 || offsetY >= MAP_HEIGHT)
        {
            // 範囲外なので終了
            return;
        }

        if (_SearchMap[offsetY, offsetX])
        {
            // すでに検索済みなので終了
            return;
        }


        _SearchMap[offsetY, offsetX] = true;


        if (_MapBlockState[offsetY, offsetX] == state)
        {
            _RemoveList.Add(new PosTbl(offsetX, offsetY));

        }
        if (_MapBlockState[offsetY, offsetX - 1] == state)
        {
            SearchTargetBlock(offsetX - 1, offsetY, state);

        }
        if (_MapBlockState[offsetY, offsetX + 1] == state)
        {
            SearchTargetBlock(offsetX + 1, offsetY, state);
        }
        if (_MapBlockState[offsetY - 1, offsetX] == state)
        {
            SearchTargetBlock(offsetX, offsetY - 1, state);
        }
        if (_MapBlockState[offsetY + 1, offsetX] == state)
        {
            SearchTargetBlock(offsetX, offsetY + 1, state);
        }

    }
}
