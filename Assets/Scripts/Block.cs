﻿using UnityEngine;

public class Block : MonoBehaviour {
    static readonly Color FRAME_COLOR = Color.black;
    static readonly Color DEFAULT_COLOR = Color.white;
    static readonly Color SKYBLUE_COLOR = new Color(0.0f, 1.0f, 1.0f);
    static readonly Color YELLOW_COLOR = Color.yellow;
    static readonly Color PURPLE_COLOR = new Color(1.0f, 0.0f, 1.0f);
    static readonly Color BLUE_COLOR = Color.blue;
    static readonly Color ORENGE_COLOR = new Color(1.0f, 0.5f, 0.0f);
    static readonly Color GREEN_COLOR = Color.green;
    static readonly Color RED_COLOR = Color.red;
    static readonly Color PLAYER_COLOR = Color.gray;

    Material _BaseMaterial = default;
    [SerializeField] Material _Material = default;
    [SerializeField] MeshRenderer _Mesh1 = default;
    [SerializeField] MeshRenderer _Mesh2 = default;


    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update() {

    }

    public void SetColor(Color color) {
        if (_BaseMaterial == null) {
            _BaseMaterial = GameObject.Instantiate<Material>(_Material);
            _Mesh1.material = _BaseMaterial;
            _Mesh2.material = _BaseMaterial;

        }

        _BaseMaterial.color = color;

    }

    public void SetState(DrillerSystem.EBlockState state) {
        bool IsActive = (state != DrillerSystem.EBlockState.kNone);
        _Mesh1.gameObject.SetActive(IsActive);
        _Mesh2.gameObject.SetActive(IsActive);

        switch (state) {
            case DrillerSystem.EBlockState.kFrame:
                SetColor(FRAME_COLOR);
                break;
            case DrillerSystem.EBlockState.kPlayer:
                SetColor(PLAYER_COLOR);
                break;
            case DrillerSystem.EBlockState.kSkyBlue:
                SetColor(SKYBLUE_COLOR);
                break;
            case DrillerSystem.EBlockState.kYellow:
                SetColor(YELLOW_COLOR);
                break;
            case DrillerSystem.EBlockState.kPurple:
                SetColor(PURPLE_COLOR);
                break;
            case DrillerSystem.EBlockState.kBlue:
                SetColor(BLUE_COLOR);
                break;
            case DrillerSystem.EBlockState.kOrenge:
                SetColor(ORENGE_COLOR);
                break;
            case DrillerSystem.EBlockState.kGreen:
                SetColor(GREEN_COLOR);
                break;
            case DrillerSystem.EBlockState.kRed:
                SetColor(RED_COLOR);
                break;
        }
    }
}
